FROM node:18.10.0-alpine3.15

ENV NODE_ENV "development"

WORKDIR /app

# install all dependencies and ignore "NODE_ENV" and use the flag
COPY package.json yarn.lock src/ tsconfig.json ./

RUN yarn install && \
    yarn build

# specifies what command to run within the container
CMD ["node", "/app/build/index.js"]
