import { access, constants } from 'fs';
import { drive_v3 } from 'googleapis/build/src/apis/drive/v3';
import { DriveFolder } from './models/drive-folder.model';

export function sleep(ms: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

export async function checkFileExists(filepath: string): Promise<boolean> {
  return new Promise((resolve) => {
    access(filepath, constants.F_OK, (error) => {
      resolve(!error);
    });
  });
}

export function getDirectoryStructure(
  dirs: drive_v3.Schema$File[],
): DriveFolder[] {
  const result: DriveFolder[] = [];
  const rootID =
    dirs.length > 0 && (dirs[0].parents?.length ?? 0) > 0
      ? dirs[0]!.parents![0]
      : 'root';

  while (dirs.length > 0) {
    const dir = dirs.splice(0, 1)[0];

    if (
      !dir.parents ||
      dir.parents.length === 0 ||
      (dir.parents.length === 1 && dir.parents[0] === rootID)
    ) {
      result.push(new DriveFolder(dir));
    } else {
      let foundAtLeastOneParent = false;
      for (const parent of dir.parents) {
        const parentDir = getDirectoryFromID(result, parent);
        if (parentDir) {
          foundAtLeastOneParent = true;
          parentDir.directories.push(new DriveFolder(dir, parentDir));
        }
      }

      if (!foundAtLeastOneParent) dirs.push(dir); // add to the end
    }
  }

  return result;
}

export function getDirectoryFromID(
  dirs: DriveFolder[],
  id: string,
): DriveFolder | undefined {
  const folders = [...dirs];
  while (folders.length > 0) {
    const dir = folders.splice(0, 1)[0];

    if (dir.dir.id === id) return dir;

    folders.push(...dir.directories);
  }

  return undefined;
}

export function getDirectoryFromPath(
  dirs: DriveFolder[],
  path: string,
): DriveFolder | undefined {
  const folders = [...dirs];
  while (folders.length > 0) {
    const dir = folders.splice(0, 1)[0];

    if (dir.getPath() === path) return dir;

    folders.push(...dir.directories);
  }

  return undefined;
}
