import { google } from 'googleapis';
import { APIEndpoint } from 'googleapis-common';
import { Credentials } from './models/credentials.model';
import { Credentials2 } from './models/credentials2.model';

export function getService(
  credentials: Credentials,
  credentials2: Credentials2,
): APIEndpoint {
  const oauth2Client = new google.auth.OAuth2(
    credentials2.installed.client_id,
    credentials2.installed.client_secret,
    credentials2.installed.redirect_uris[0],
  );
  oauth2Client.setCredentials({
    access_token: credentials.access_token,
    refresh_token: credentials.refresh_token,
    scope: credentials.scope,
    expiry_date: credentials.expiry_date,
  });

  return google.drive({
    version: 'v3',
    auth: oauth2Client,
  });
}
