export type Credentials = {
  access_token: string;
  refresh_token: string;
  scope: string;
  expiry_date: number;
};
