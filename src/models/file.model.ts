const REGEX_FILE_SPLITTER = /[\\/]/gi;

export class File {
  public readonly originalFile: string;
  public readonly fileName: string;
  public readonly folders: string[];

  public get folderPath(): string {
    return this.folders.join('/');
  }

  public constructor(input: string) {
    this.originalFile = input;
    this.folders = input.split(REGEX_FILE_SPLITTER);
    this.fileName = this.folders.pop()!;
  }
}
