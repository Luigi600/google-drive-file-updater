import { drive_v3 } from 'googleapis/build/src/apis/drive/v3';

export class DriveFolder {
  public readonly dir: drive_v3.Schema$File;
  public readonly directories: DriveFolder[] = [];
  public readonly parent: DriveFolder | undefined;

  public constructor(
    dir: drive_v3.Schema$File,
    parent: DriveFolder | undefined = undefined,
  ) {
    this.dir = dir;
    this.parent = parent;
  }

  public getPath(): string {
    let result = this.dir.name!;

    let parent = this.parent;
    while (parent) {
      result = `${parent.dir.name}/${result}`;

      parent = parent.parent;
    }

    return result;
  }

  public toJSON(): Object {
    return {
      name: this.dir.name,
      directories: this.directories.map((d) => d.toJSON()),
    };
  }
}
