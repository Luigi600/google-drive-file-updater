export type Credentials2 = {
  installed: {
    client_id: string;
    client_secret: string;
    redirect_uris: string[];
  };
};
