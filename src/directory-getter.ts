import { APIEndpoint } from 'googleapis-common';
import { drive_v3 } from 'googleapis/build/src/apis/drive/v3';
import { DriveFolder } from './models/drive-folder.model';
import { getDirectoryStructure } from './util';

export async function getFolders(drive: APIEndpoint): Promise<DriveFolder[]> {
  const folderList: drive_v3.Schema$File[] = [];
  let pageToken: string | undefined = undefined;
  while (true) {
    const foldersRes = (await drive.files.list({
      q: "mimeType = 'application/vnd.google-apps.folder'",
      fields: 'nextPageToken, files',
      pageToken,
    })) as drive_v3.Schema$FileList as any;

    if (foldersRes.data && foldersRes.data.files)
      folderList.push(...foldersRes.data.files);

    if (!foldersRes.data.nextPageToken) break;

    pageToken = foldersRes.data.nextPageToken;
  }

  return getDirectoryStructure(folderList);
}
