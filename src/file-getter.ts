import { exit } from 'process';
import { APIEndpoint } from 'googleapis-common';
import { drive_v3 } from 'googleapis/build/src/apis/drive/v3';
import { getDirectoryFromPath } from './util';
import { DriveFolder } from './models/drive-folder.model';
import { File } from './models/file.model';

export async function getDriveFile(
  service: APIEndpoint,
  dirStructure: DriveFolder[],
  file: File,
): Promise<drive_v3.Schema$File> {
  const parent = getDirectoryFromPath(dirStructure, file.folderPath);
  if (!parent) {
    console.error("Invalid path! Path doesn't exist!");
    exit(1);
  }

  const fileInDrive = await service.files.list({
    q: `name='${file.fileName}' and trashed=false and '${
      parent!.dir.id
    }' in parents`,
    fields: 'files',
  });

  if (!fileInDrive.data || fileInDrive.data.files.length === 0) {
    console.error("Invalid path! Path doesn't exist!");
    exit(1);
  }

  return fileInDrive.data.files[0];
}
