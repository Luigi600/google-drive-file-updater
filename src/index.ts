import { exit } from 'process';
import minimist from 'minimist';
import { createReadStream } from 'fs';
import { readFile } from 'fs/promises';
import { checkFileExists } from './util';
import { getService } from './service-getter';
import { getFolders } from './directory-getter';
import { getDriveFile } from './file-getter';
import { File } from './models/file.model';

// eslint-disable-next-line @typescript-eslint/no-var-requires
require('console-stamp')(console, {
  format: ':date(yyyy/mm/dd HH:MM:ss.l)',
});

async function main(): Promise<void> {
  const argv = minimist(process.argv.slice(2));
  const fileToUpload = argv.f;
  const fileNameInDrive = argv.t;
  const credentialsFile = argv.credentials;
  const credentialsFile2 = argv.credentials2;
  if (!fileToUpload || !(await checkFileExists(fileToUpload))) {
    console.error("Invalid input file! File doesn't exist!");
    exit(1);
  }

  if (!credentialsFile || !(await checkFileExists(credentialsFile))) {
    console.error('Invalid credentials file!');
    exit(1);
  }

  if (!credentialsFile2 || !(await checkFileExists(credentialsFile2))) {
    console.error('Invalid credentials2 file!');
    exit(1);
  }

  if (!fileNameInDrive) {
    console.error('Invalid target path!');
    exit(1);
  }

  const file = new File(fileNameInDrive);
  const service = getService(
    JSON.parse((await readFile(credentialsFile)).toString()),
    JSON.parse((await readFile(credentialsFile2)).toString()),
  );
  const dirStructure = await getFolders(service);
  const fileInDrive = await getDriveFile(service, dirStructure, file);

  // upload
  const result = await service.files.update({
    fileId: fileInDrive.id,
    media: {
      body: createReadStream(fileToUpload),
    },
  });

  if (result.status !== 200) {
    console.error(result);
    exit(1);
  }

  console.info('File updated successfully!');
}

main();
